import queue
import sys

class BinaryTree():
    def __init__(self, value):
        self.value = value
        self.left  = None
        self.right = None


    def BST_insert(self, value):
        if value < self.value and self.left:
            self.left.BST_insert(value)
        elif value < self.value:
            self.left = BinaryTree(value)
        elif value > self.value and self.right:
            self.right.BST_insert(value)
        elif value > self.value:
            self.right = BinaryTree(value)
        else:
            print("UNHANDLED CASE OCCURED in BST INSERT")

    def BST_search(self, value):
        print("Search")
        if value < self.value and self.left:
            node = self.left.BST_search(value)
            return node
        elif value > self.value and self.right:
            node = self.right.BST_search(value)
            return node
        elif value == self.value:
            print("Found it..!")
            return self
        else:
            print("Not Found!!")
            return None
    def removeNode(self):
        print("Removing Node with Value %d" %(self.value))
        self.value = None
        self.left = None
        self.right = None
        return
    def findTheSmallest(self):
        if self.left :
            print("Reached at : %d" %(self.value))
            node = self.left.findTheSmallest()
            return node
        else:
            return self
    def BST_delete(self, value, parent):
        if value < self.value and self.left:
            self.left.BST_delete(value, self)
        elif value < self.value:
            return False
        elif value > self.value and self.right:
            self.right.BST_delete(value, self)
        elif value > self.value:
            return False
        elif value == self.value:
            if self.left is None and self.right is None:
                if self == parent.left:
                    parent.left = None
                    self.removeNode()
                elif self == parent.right:
                    parent.right = None
                    self.removeNode()
                else:
                    print("Unhandled Case occured")
            elif self.left and self.right is None:
                if self == parent.left:
                    parent.left = self.left
                    self.removeNode()
                elif self == parent.right:
                    parent.right = self.left
                    self.removeNode()
                else:
                    print("Unhandled Case occured")
            elif self.right and self.left is None:
                if self == parent.left:
                    parent.left = self.right
                    self.removeNode()
                elif self == parent.right:
                    parent.right = self.right
                    self.removeNode()
                else:
                    print("Unhandled Case occured")
            elif self.right and self.left:
                if self == parent.left:
                    parent.left = self.right.findTheSmallest()
                    self.removeNode()
                elif self == parent.right:
                    parent.right = self.right.findTheSmallest()
                    self.removeNode()
                else:
                    print("Unhandled Case occured")

    def insert_left(self, value):
        if(None == self.left):
            self.left = BinaryTree(value)
        else:
            new_node = BinaryTree(value)
            new_node.left = self.left
            self.left = new_node

    def insert_right(self, value):
        if(None == self.right):
            self.right = BinaryTree(value)
        else:
            new_node = BinaryTree(value)
            new_node.right = self.right
            self.right = new_node

    def print_preorder_dfs(self):
        print(self.value)

        if self.left:
            self.left.print_preorder_dfs()
        if self.right:
            self.right.print_preorder_dfs()

    def print_inorder_dfs(self):
        if self.left:
            self.left.print_inorder_dfs()
        print(self.value)
        if self.right:
            self.right.print_inorder_dfs()

    def print_postorder_dfs(self):
        if self.left:
            self.left.print_postorder_dfs()
        if self.right:
            self.right.print_postorder_dfs()
        print(self.value)

    def print_bfs(self):
        q = queue.Queue()
        q.put(self)

        while not q.empty():
            current_node = q.get()
            print(current_node.value)

            if current_node.left:
                q.put(current_node.left)

            if current_node.right:
                q.put(current_node.right)

root = BinaryTree(8)

#/*****NON BST****/
# root.insert_left(4)
# root.insert_right(12)

# traverser = root.left
# traverser.insert_left(2)
# traverser.insert_right(5)

# traverser = root.right
# traverser.insert_left(9)
# traverser.insert_right(13)


# print(root.value)
# print(root.left.value)
# print(root.left.left.value)
# print(root.left.right.value)
# print(root.right.value)
# print(root.right.left.value)
# print(root.right.right.value)


# print("Pre Order DFS")
# root.print_preorder_dfs()
# print("In Order DFS")
# root.print_inorder_dfs()
# print("Post Order DFS")
# root.print_postorder_dfs()
# print("BFS")
# root.print_bfs()


root.BST_insert(4)
root.BST_insert(2)
root.BST_insert(12)
root.BST_insert(19)
root.BST_insert(25)
root.BST_insert(97)
root.BST_insert(11)
root.BST_insert(17)
root.BST_insert(-69)
# root.print_preorder_dfs()
if len(sys.argv) > 1:
    print("Searching")
    
    node = root.BST_search(int(sys.argv[1]))
    print("Value: %d" %(node.value))
    
    node = root.findTheSmallest()
    print("Smallest: %d" %(node.value))

    root.print_preorder_dfs()
    root.BST_delete(int(sys.argv[1]), None)
    root.print_preorder_dfs()
else:
    print("No Argument provided")
