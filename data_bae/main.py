#   O   O   O   O   O   O   O   O
#   x   x           x           x
#   3
#
#I  1   1   1   1   0   0   0   0
#O  1100
#   
#R  n/2 left- n/2-(num of 1s in output) 
#R  n/2 right- n/2(+1)-(num of 0s in output)
#
# Two Strings 
#   index offser 0  index offser 4
#   1   1   1   1   0   0   0   0
#   2 kharaab       2 kharaab
#   
#I  1   1   0   0   1   1   0   0
#O  00              10
#   0 and 1          
#I  0   0   0   0   1   0   1   0
#O  00              01
#                   index offser + 0 and index offser + 1



#INPUT = 11110000 Output= 1100
import sys, math


class Findings():

    def __init__(self):
        self.lower = None
        self.upper = None
        self.broken = None
        
        self.left = None
        self.right = None

    def print_node(self):
        print("Node: (%d,%d) Broken:%d" %(self.lower, self.upper, self.broken))

num_test_cases = 0

num_of_workers   = 0
num_of_b_workers = 0
tries            = 0

index_of_broken_workers = []


findings_list = []
first_finding_flag = False

def count_ones(input):
    return(input.count('1'))

def count_zeroes(input):
    return(input.count('0'))

def input_handler():
    global num_of_workers, num_of_b_workers, tries

    num_of_workers = int(input())
    num_of_b_workers = int(input())
    tries = int(input())

def send_test_input(input):
    print(input)
    sys.stdout.flush()

def read_workers():
    i = input()
    return (i)

def generate_input_for_workers(size):
    #Generates 1010101010
    count = 0
    input = [0]*size
    for count in range(0, int(size/2)):
        input[count] = 1
    input = ''.join(str(e) for e in input)

    return (input)

def add_broken_worker_to_list(index):
    global index_of_broken_workers
    index_of_broken_workers.append(index)

def find_broken_ones(n, b, f):
    global input_to_worker, output_from_worker, first_finding_flag
    tries_counter = 1
    input_to_worker = [0] * n
    output_from_worker = [0] * n
    #Algo
    while tries_counter - 1 != f:    
        input_to_worker = generate_input_for_workers(int(n/tries_counter))
        for count in range (1, tries_counter):
            input_to_worker += input_to_worker

        #send test Input to Workers
        send_test_input(input_to_worker)
        #read from Workers
        output_from_worker = read_workers()
        # print(output_from_worker)

        
        findings_toggle_flag = False
        for count in range(0, pow(2,tries_counter)):
            f = Findings()
            #Find the range
            f.lower = 0 + (count *  ( n/pow(2, tries_counter ) ))
            f.upper = n/pow(2, tries_counter) + (count *  ( n/pow(2, tries_counter) )) - 1


            if True == first_finding_flag:
                #Get string characters in that range
                str1 = input_to_worker[int(f.lower):int(f.upper)+1]
                if False == findings_toggle_flag:
                    f.broken = count_ones(str1)
                    findings_toggle_flag = True
                    #gotta use f.broken of previous case
                    str2 = output_from_worker[int(f.lower): (int(f.upper) - int(f.broken) + 1)]
                else:
                    f.broken = count_zeroes(str1)
                    findings_toggle_flag = False
                    str2 = output_from_worker[int(f.lower): (int(f.upper) - int(f.broken) + 1)]
            else:
                print("First Findings Is False")
                if False == findings_toggle_flag:
                    str1 = input_to_worker[0: int(n/2)]
                    #Get string characters after first ocurence of 0
                    f.broken = count_ones(input_to_worker) - count_ones(output_from_worker)
                    str2 = output_from_worker[int(f.lower) : int(f.upper) - int(f.broken) + 1]
                    findings_toggle_flag = True
                else:
                    str1 = input_to_worker[int(n/2): n]
                    #Get string characters after first ocurence of 0
                    f.broken = count_zeroes(input_to_worker) - count_zeroes(output_from_worker)
                    str2 = output_from_worker[int(f.lower) - 1 : int(f.upper) - int(f.broken)]
                    findings_toggle_flag = False
                    first_finding_flag = True
                    print("First Findings set to True")
            print("(%d, %d) Broken %d stri %s stro %s:" %(f.lower, f.upper, f.broken, str1, str2))
        #increment tries
        tries_counter = tries_counter + 1

num_test_cases = int(input())
for count in range(0, num_test_cases):
    input_handler()
    find_broken_ones(num_of_workers, num_of_b_workers, tries)